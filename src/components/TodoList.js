//import liraries
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const TodoList = ({todos, toggleTodo}) => (
    <View style={{padding:20}}>
        {todos.map(todo => {
            return(
            <TouchableOpacity
                key={todo.id}
                onPress={() => toggleTodo(todo.id)}
                style={{backgroundColor:'#991111', padding:15, marginTop:10}}
            > 
                <Text style={{textDecorationLine:todo.completed?'line-through':'none', color:'white'}}>{todo.text}</Text>
            </TouchableOpacity>
            )
        })}
    </View>
)
export default TodoList;
