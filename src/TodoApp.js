//import liraries
import React, { Component } from 'react';
import { View} from 'react-native';
import AddTodo from './containers/AddTodo'
import  VisibleTodos from './containers/VisibleTodos'
import  Countries from './containers/Countries'

// create a component
class TodoApp extends Component {
    render() {
        return (
            <View style={{flex:1, paddingTop:35}}>
                <AddTodo />
                <VisibleTodos />
                <Countries />
            </View>
        );
    }
}

export default TodoApp;
