//import liraries
import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';
import getCountries from "../actions"
class AddTodo extends Component {
    constructor(props){
        super(props);
        this.state = {
            text:''
        }
    }
    
    addTodo = (text) => {
        // update redux store here 
        this.props.dispatch({type:'ADD_TODO', text})
        this.setState({text:''});
    }
    componentDidMount(){
        console.log('todo comp')
        this.props.getCountries();
    }
    render() {
        return (
            <View style={{flexDirection:'row'}}>
                <TextInput 
                    placeholder="Todo List Name"
                    style={{flex:1, backgroundColor:'#e7e7e7', padding:15}}
                    value={this.state.text}
                    onChangeText = {(t) => this.setState({text:t})}
                />
                <TouchableOpacity style={{padding:15, backgroundColor:'orange'}}
                    onPress={() => this.addTodo(this.state.text)}
                >
                    <Text>Add</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    todos:state.todos
})
//make this component available to the app
export default connect(mapStateToProps, {getCountries})(AddTodo);
