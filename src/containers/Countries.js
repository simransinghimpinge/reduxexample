//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {connect} from 'react-redux'
// create a component
class Countries extends Component {
    constructor(props){
        super(props);
        this.state ={
            countries: this.props.countries
        }
    }
    render() {
        let {countries} = this.state;
        return (
            <View style={styles.container}>
                <Text>Countries</Text>
                <Text>{JSON.stringify(countries)}</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});
const mapStateToProps = state =>{
    console.log('state', state.countries);
    return {countries: state.countries};
}
//make this component available to the app
export default connect(mapStateToProps)(Countries);

