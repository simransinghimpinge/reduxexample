import {combineReducers} from 'redux';
import todos from './todos'
import countries from './CountryData'
import visibilityFilter from './visibilityFilter'

export default combineReducers({
    todos,
    visibilityFilter,
    countries
})